# Amazon reviews: back-to-back metrics study with MLflow

## Preprocessing
Dataset preprocessing steps include:
- merging the summary and the main text into one line (missing summaries count as empty strings);
- casting to lowercase;

Labels were fixed from 1-2 to a conventional 0-1 set. Labels are roughly balanced, no stratification was used.

10% of the data selected for the test set.

## Vectorizing
We test the following vectorizing methods:
- BoW (`CountVectorizer()`);
- TF-IDF (`TfidfVectorizer()`).

In each case, stop-words were removed (using sklearn built-in 'english' dict).

100 most popular tokens were selected as a compromize between performance and accuracy.

In addition, both words and words+bigrams approaches were tested (bigram-only performs much worse, though the addition of stemming/lemmatization might improve that).

The resulting vectors are further normalized (using sklearn `Normalizer()`) to avoid any problems with gradient descent methods.

## Machine Learning models
This experiment includes:

- robust logistic regression (`SGDClassifier()` using modified Huber loss);
- gradient boosting machine (`HistGradientBoostingClassifier()`) with a fixed tree height of 2.

The number of iterations is limited to 100 in both cases for the reasonable training times.

Basic regularized logistic regression, naive Bayes and LDA are also supported out of the box.

## Evaluation
Model evaluation is done by 5 fold cross-validation during the train stage. Log loss and Brier score are calculated for each fold (box plots available via MLflow artifacts).

Presentation metrics sample is based upon a simple best F1 decision threshold. The recall and precision for the default threshold are supplied for comparison.

ROC and PR curves for the positive class are also available via MLflow artifacts.

## Evaluation results
### Proper scoring rules
![CV results](cv.png)
GBM models display notably smaller log loss while the Brier score and other metrics tend to favor a linear model. This might indicate boosting model being better calibrated. Further calibration studies due.

### Threshold independent positive class metrics
![CV results](auc.png)
Both ROC_AUC and AP (PR_AUC) favor the linear model very slightly but steadily (about 1-2%), indicating somewhat better performance on the positive class.

### 'Intuitive' metrics
![CV results](pr.png)
Same as above, both accuracy and the optimal F1 for the positive class favor the linear model, reaching around 0.85 recall and 0.68 precision, yet the GBM model is still just about 1-2% behind.

## Conclusion
Overall, the linear model tends to be very slightly ahead as is, making it more cost effective. The GBM model still has a room for improvement with more/higher trees at the cost of further increase in computational complexity.

Additional studies on calibration recommended.

Vectorizer settings show no major effects, the linear model having insignificantly better performance with BoW and GBM performing insignificantly better with TF-IDF.
