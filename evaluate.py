import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from joblib import load

from sklearn.metrics import (
    accuracy_score,
    recall_score,
    precision_score,
    f1_score,
    roc_auc_score,
    roc_curve,
    precision_recall_curve,
    average_precision_score,
)

import mlflow


def calculate_metrics(target_test, probabilities):
    predictions = probabilities > 0.5
    ap = average_precision_score(target_test, probabilities)
    roc_auc = roc_auc_score(target_test, probabilities)

    precision, recall, thresholds = precision_recall_curve(
        target_test,
        probabilities,
    )
    # You may adjust the beta here:
    beta = 1
    f_scores = (
        (1 + beta**2)
        * recall
        * precision
        / (recall + beta**2 * precision + np.finfo(float).eps)
    )
    best_thresh = thresholds[np.argmax(f_scores)]
    best_f = np.max(f_scores)

    optimal_threshold_preds = probabilities > best_thresh

    mlflow.set_tracking_uri(snakemake.params.mlflow_url)
    mlflow.set_experiment("Amazon Reviews")
    with mlflow.start_run(
        run_name=f"{snakemake.params.model}-{snakemake.params.vectorizer}-{snakemake.params.max_features}-{snakemake.params.ngram_range}"
    ):
        mlflow.log_param("Vectorizer", snakemake.params.vectorizer)
        mlflow.log_param("Vectorizer features", snakemake.params.max_features)
        mlflow.log_param(
            "Vectorizer N-gram range", snakemake.params.ngram_range
        )
        mlflow.log_param("Model", snakemake.params.model)
        mlflow.log_param("Model max iterations", snakemake.params.max_iter)

        cv_logloss = load(snakemake.input.logloss)
        cv_brier = load(snakemake.input.brier)
        mlflow.log_metric("CV Log loss", cv_logloss.mean())
        mlflow.log_metric("CV Brier score", cv_brier.mean())
        mlflow.log_metric(
            "Accuracy at 0.5",
            accuracy_score(target_test, predictions),
        )
        mlflow.log_metric(
            "Accuracy at the optimal threshold",
            accuracy_score(target_test, optimal_threshold_preds),
        )
        mlflow.log_metric(
            "Recall at 0.5", recall_score(target_test, predictions)
        )
        mlflow.log_metric(
            "Recall at the optimal threshold",
            recall_score(target_test, optimal_threshold_preds),
        )
        mlflow.log_metric(
            "Precision at 0.5",
            precision_score(target_test, predictions),
        )
        mlflow.log_metric(
            "Precision at the optimal threshold",
            precision_score(target_test, optimal_threshold_preds),
        )
        mlflow.log_metric("ROC_AUC", roc_auc)
        mlflow.log_metric("AP", ap)
        mlflow.log_metric("F1 at 0.5", f1_score(target_test, predictions))
        mlflow.log_metric("F1 at the optimal threshold", best_f)
        mlflow.log_metric("Optimal threshold", best_thresh)

        fig, axes = plt.subplots(1, 2, figsize=(10, 4))
        axes[0].plot([0, 1], linestyle="--")
        axes[1].plot([0.5, 0.5], linestyle="--")
        fpr, tpr, _ = roc_curve(target_test, probabilities)
        precision, recall, _ = precision_recall_curve(
            target_test, probabilities
        )
        axes[0].plot(fpr, tpr)
        axes[1].plot(recall, precision)

        axes[0].set(
            xlabel="FPR",
            ylabel="TPR",
            title="ROC curve",
            xlim=(0, 1),
            ylim=(0, 1),
        )
        axes[1].set(
            xlabel="Recall",
            ylabel="Precision",
            title="PR curve",
            xlim=(0, 1),
            ylim=(0, 1),
        )
        plt.savefig(snakemake.output.metrics, bbox_inches="tight")
        mlflow.log_figure(fig, "curves.png")
        fig, ax = plt.subplots()
        ax.boxplot(cv_logloss, vert=False)
        plt.title("Cross-validation: Log Loss")
        mlflow.log_figure(fig, "Log Loss.png")
        fig, ax = plt.subplots()
        ax.boxplot(cv_brier, vert=False)
        plt.title("Cross-validation: Brier Score")
        mlflow.log_figure(fig, "Brier Score.png")


vectorizer = load(snakemake.input.vectorizer)
model = load(snakemake.input.model)

val_data = pd.read_csv("data/test.csv")

X_test = vectorizer.transform(val_data["full_text"]).toarray()
y_test = val_data["label"]

probs = model.predict_proba(X_test)[:, 1]

calculate_metrics(y_test, probs)
