import pandas as pd

from sklearn.preprocessing import Normalizer
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.pipeline import Pipeline
from joblib import dump
from ast import literal_eval

vectorizer_method = snakemake.params.vectorizer
scaler_method = snakemake.params.scaling
max_features = int(snakemake.params.max_features)
ngram_range = literal_eval(snakemake.params.ngram_range)

model_map = {
    "bow": CountVectorizer,
    "tfidf": TfidfVectorizer,
}

# QA: very few methods work with sparse matrices.
scale_map = {
    "length": Normalizer(),
}

vectorizer = Pipeline(
    [
        (
            "vectorize",
            model_map.get(vectorizer_method)(
                stop_words="english",
                ngram_range=ngram_range,
                max_features=max_features,
            ),
        ),
        ("scale", scale_map.get(scaler_method, "length")),
    ]
)

train_data = pd.read_csv("data/train.csv")

vectorizer.fit(train_data["full_text"])

dump(vectorizer, snakemake.output.vectorizer)
