import pandas as pd

df = pd.read_csv("raw/data.csv", header=None)

df.columns = ["label", "summary", "text"]

# There are 2 text columns. We assume the vocabulary is the same and merge those.
df["full_text"] = df["summary"].fillna("") + " " + df["text"]
df["full_text"] = df["full_text"].str.strip().str.lower()

# Originally, 2 is positive and 1 is negative (mostly balanced). We transform those to more common 1 and 0.
df["label"] = df["label"] - 1

df_test = df.sample(
    frac=snakemake.params.test_size,
    random_state=snakemake.params.random_seed,
)

df_train = df.copy()[~df.index.isin(df_test.index)]

df_train[["full_text", "label"]].to_csv("data/train.csv", index=False)
df_test[["full_text", "label"]].to_csv("data/test.csv", index=False)
