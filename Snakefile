from multiprocessing import cpu_count
from snakemake.report import auto_report


data_csv = "raw/data.csv"
random_seed = 177013
mlflow_url = "http://127.0.0.1:8000"
test_size = 0.2
thread_count = lambda cores: max(1, cpu_count() // 2)

vectorizer_list = ['bow', 'tfidf']
model_list = ['sgd', 'gbm']
max_features_list = [100]
ngram_range_list = [(1,1), (1,2)]

max_iter = 100


rule all:
    input:
        metrics=expand('report/metrics/{model}_{vectorizer}_{max_features}_{ngram_range}.png', model=model_list, vectorizer=vectorizer_list, max_features=max_features_list,ngram_range=ngram_range_list),

rule preprocess:
    input:
        dataset=data_csv
    output:
        train='data/train.csv',
        test='data/test.csv'
    params:
        random_seed=random_seed,
        test_size=test_size,
    threads:
        thread_count
    script:
        "train_test_split.py"

rule train_vectorizer:
    input:
        train='data/train.csv',
    output:
        vectorizer = 'model/{vectorizer}_{max_features}_{ngram_range}/vectorizer.joblib'
    params:
        ngram_range='{ngram_range}',
        max_features='{max_features}',
        scaling='length',
        vectorizer='{vectorizer}'
    threads:
        thread_count
    script:
        "vectorize.py"

rule train:
    input:
        train='data/train.csv',
        vectorizer = 'model/{vectorizer}_{max_features}_{ngram_range}/vectorizer.joblib'
    output:
        model='model/{vectorizer}_{max_features}_{ngram_range}/model_{model}.joblib',
        logloss='model/{vectorizer}_{max_features}_{ngram_range}/{model}_cv_logloss.joblib',
        brier='model/{vectorizer}_{max_features}_{ngram_range}/{model}_cv_brier.joblib',
    params:
        random_seed=random_seed,
        max_iter=max_iter,
        model='{model}'
    threads:
        thread_count
    script:
        "train.py"

rule evaluate:
    input:
        test='data/test.csv',
        model='model/{vectorizer}_{max_features}_{ngram_range}/model_{model}.joblib',
        vectorizer = 'model/{vectorizer}_{max_features}_{ngram_range}/vectorizer.joblib',
        logloss='model/{vectorizer}_{max_features}_{ngram_range}/{model}_cv_logloss.joblib',
        brier='model/{vectorizer}_{max_features}_{ngram_range}/{model}_cv_brier.joblib',
    output:
        metrics='report/metrics/{model}_{vectorizer}_{max_features}_{ngram_range}.png',
    params:
        random_seed=random_seed,
        mlflow_url=mlflow_url,
        vectorizer='{vectorizer}',
        model='{model}',
        max_iter=max_iter,
        ngram_range='{ngram_range}',
        max_features='{max_features}'
    threads:
        thread_count
    script:
        "evaluate.py"

onsuccess:
    with open("report/dag.txt","w") as f:
        f.writelines(str(workflow.persistence.dag))
    shell("cat report/dag.txt | dot -Tsvg > report/dag.svg")
