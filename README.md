# MLops homework: MLflow

This is a homework repository for the [MLOps & production for data science research 3.0](https://ods.ai/tracks/mlops3-course-spring-2024).

This one includes the code and report only - no data, artifacts or CI. Check out `report/report.md`.

Dropping Amazon reviews `train.csv` into `raw/` should result in a reproducible Snakemake pipeline nevertheless. See docs/dag.md for the graph and description.


## Running:

MLflow is assumed be running locally on port 8000 (controlled by `mlflow_url` parameter).

`conda env create -f dev.yaml`

`conda run -n dev snakemake --cores all`
