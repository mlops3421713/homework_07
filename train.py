import pandas as pd

from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.ensemble import HistGradientBoostingClassifier

from joblib import dump, load

models = {
    "lr": LogisticRegression(
        random_state=snakemake.params.random_seed,
        max_iter=snakemake.params.max_iter,
    ),
    "sgd": SGDClassifier(
        loss="modified_huber",
        random_state=snakemake.params.random_seed,
        n_jobs=snakemake.threads,
        max_iter=snakemake.params.max_iter,
    ),
    "nb": GaussianNB(),
    "lda": LinearDiscriminantAnalysis(),
    "gbm": HistGradientBoostingClassifier(
        random_state=snakemake.params.random_seed,
        max_iter=snakemake.params.max_iter,
        max_depth=2,
    ),
}

model = models.get(
    snakemake.params.model,
    LogisticRegression(
        random_state=snakemake.params.random_seed,
        max_iter=snakemake.params.max_iter,
    ),
)

vectorizer = load(snakemake.input.vectorizer)

train_data = pd.read_csv(snakemake.input.train)

X_train = vectorizer.transform(train_data["full_text"]).toarray()
y_train = train_data["label"]

model.fit(X_train, y_train)

dump(model, snakemake.output.model)

cv_logloss = -cross_val_score(
    model, X_train, y_train, cv=5, scoring="neg_log_loss"
)
cv_brier = -cross_val_score(
    model, X_train, y_train, cv=5, scoring="neg_brier_score"
)

dump(cv_logloss, snakemake.output.logloss)
dump(cv_brier, snakemake.output.brier)
